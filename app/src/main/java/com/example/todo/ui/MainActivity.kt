package com.example.todo.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todo.R
import com.example.todo.databinding.ActivityMainBinding
import com.example.todo.models.Task
import com.example.todo.ui.adapter.TaskAdapter

class MainActivity : AppCompatActivity() {

    private val tasks: MutableList<Task> = mutableListOf();
    private val taskAdapter = TaskAdapter(tasks)
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )

        setupView()
    }

    private fun setupView() {
        binding.recyclerViewTasks.layoutManager = LinearLayoutManager(applicationContext)
        binding.recyclerViewTasks.adapter = taskAdapter
        binding.btnAdd.setOnClickListener {
            tasks.add(Task(binding.inputTaskName.text.toString()))
            taskAdapter.notifyDataSetChanged()
        }
    }

}